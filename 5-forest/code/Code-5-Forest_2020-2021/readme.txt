(1) To begin with, you need the full data folders available at : https://www.dropbox.com/sh/0shjz7amy3qqb4j/AAAt78XQ_6DLSmqI9FBktLwwa?dl=0 








(2) The files in lidar_features/raster have been slightly modified to include a "plotid" column in each. Therefore, you need to erase the "raster" folder initially provided and replace it with the "raster" folder provided in this zip archive.








(3) Run the CSV_total.py script: it will generate the CSV file used to train the Random Forest Agorithm, named "data_train_full". It might take a while. To run this script, there are two ways: on Jupyter Notebook (we wrote our script on it) or on Spyder (among other things, you will have to change the "/" to "\" in the program's file addresses). 
"Premol" may not work, so you will need to comment on the "Premol" parts first, to make the code work and understand it. The error displayed is an error in the number of rows and columns of the "premol" image (type 110 is out of bounds 102). In order to integrate the image into the project, it is possible to modify the code to adapt to the image format. The "df_pixel2.0" file is identical to the initial file but without the lines corresponding to the "premol" image. In the future, to execute the code including "premol", it will be necessary to think of putting the "df_pixel" file in its place. 








(4) After that, you (might) need to delete the line 12853 of the CSV file because of an error. (specie = 'ND'). This was our case, which is why we wrote line 153 of the "CSV_total" program. Try uncommenting and commenting on it.








(5) To run the RF algorithm and display the classification map, run the RandomForest.py script. As for "CSV_total", use either Notebook Jupyter without modification, or Spyder by adapting the code.








(6) Bonus : You can use the "data_train_full.csv" file we created to test RandomForest directly if you want. 


(7) Bonus 2 : You have a Jupyter Notebook that runs all the code in image 1, which you can use to understand how the programs work.
















Note : 'whittaker.py' and 'tif_picture_to_dataframe' should not be modified. 








CAUTION : All the python file need to be at the same level that the 'gt', 'hi', 'lidar' and 'lidar_features' folders.








NEEDED MODULES : pandas, numpy, sklearn, tifffile, matplotlib (+eventually seaborn)