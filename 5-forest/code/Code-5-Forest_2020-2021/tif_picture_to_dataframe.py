import numpy as np
import tifffile
import pandas as pd
import whittaker



def tiff_to_dataframe(plotid):
    """plotid : 1, 1b, 2, ..., Premol
    
    Turn a tif picture into a pandas Dataframe with columns : [row, col, Band1, ..., Band160]"""
    
    
    hi_tiff = tifffile.TiffFile(r"hi/{}.tif".format(plotid))
    hi_arr = hi_tiff.asarray()
    hi_metadata = hi_tiff.shaped_metadata[0]
    
    n_cols = hi_metadata["ncols"]
    n_rows = hi_metadata["nrows"]
    n_bands = len(hi_metadata["wavelength"])
    
    
    #Smooth reflectance using whittaker algorithm
    
    for r in range(hi_arr.shape[0]):
        print("Smoothing hyperspectral image : {}%".format(round((r/n_rows)*100), 2))
        for c in range(hi_arr.shape[1]):
            hi_arr[r,c] = whittaker.whittaker_smooth(hi_arr[r,c], 12)
    print("Smoothing hyperspectral image : done")
            
    #Turn the tif picture into a pandas dataframe
    
    x,y,z = hi_arr.shape
    out_arr = np.column_stack((np.repeat(np.arange(x),y),hi_arr.reshape(x*y,-1)))
    out_df = pd.DataFrame(out_arr)

    out_df = out_df.drop(columns = [0])
    
    list_columns = ["Bande"+str(i) for i in range(1,n_bands+1)]
    
    out_df.columns = list_columns
    
    list_rows = []
    list_cols = []
    
    for r in range(n_rows):
        list_rows += [r]*n_cols
    
    for c in range(n_rows):
        list_cols += [i for i in range(n_cols)]
        
        
    list_columns = ["Bande"+str(i) for i in range(n_bands)]
    
    
    out_df.insert(loc=0, column='col', value=list_cols)
    out_df.insert(loc=0, column='row', value=list_rows)
    
    
    
    return out_df

def add_indices(plotid):
    
    """Compute 21 indices related to the hyperspectral data. See: Individual Tree Crown Segmentation 
    and Classification of 13 Tree Species Using Airborne Hyperspectral Data, Julia Maschler, Clement 
    Atzberger and Markus Immitzer"""
    
    df = tiff_to_dataframe(plotid)
    
    df['RVI'] = df["Bande107"]/df["Bande73"]
    df['NIRR'] = df["Bande99"]-df["Bande69"]
    df['NDVI'] = (df['Bande114']-df['Bande70'])/(df['Bande114']+df['Bande70'])
    df['GRR'] = (df['Bande41']-df['Bande69'])/(df['Bande41']+df['Bande69'])
    df['DD'] = (2*df['Bande147'] - df['Bande93']) - (df['Bande65']-df['Bande37'])
    df['ARVI'] = (df['Bande124']-2*df['Bande68']-df['Bande14'])/(df['Bande124']+2*df['Bande68']+df['Bande14'])
    df['GARI'] = (df['Bande92']-(df['Bande36']-(df['Bande14']-df['Bande36'])))/(df['Bande92']+(df['Bande36']-(df['Bande14']-df['Bande36'])))
    df['GNDVI'] = (df['Bande93']-df['Bande37'])/(df['Bande93']+df['Bande37'])
    #df['VARI'] = (df['Bande41']-['Bande69'])/(df['Bande41']+['Bande69']+df['Bande21'])
    df['EVI1'] = 2.5*((df['Bande125']-df['Bande65'])/(df['Bande125']+ 6*df['Bande65'] -7.5* df['Bande15'] + 1))
    df['EVI2'] = 2.5*((df['Bande125']-df['Bande65'])/(df['Bande65']*2.4+df['Bande125']+1))
    df['PRI'] = (df['Bande33']-df['Bande43'])/(df['Bande33']+df['Bande43'])
    df['RENDVI'] = (df['Bande93']-df['Bande81'])/(df['Bande93']+df['Bande81'])
    df['mSRI'] = (df['Bande93']-df['Bande9'])/(df['Bande81']-df['Bande9'])
    df['mND'] = (df['Bande93']-df['Bande81'])/(df['Bande93']+df['Bande81']-2*df['Bande9'])
    df['GR'] = df['Bande37']/df['Bande69']
    df['BR'] = (df['Bande69']/df['Bande19']) * (df['Bande37']/df['Bande19']) * (df['Bande85']/df['Bande19']) * (df['Bande115']/df['Bande19'])
    df['RR'] = (df['Bande114']/df['Bande68']) * (df['Bande36']/df['Bande68']) * (df['Bande114']/df['Bande84'])
    df['IPVI'] = df['Bande106']/(df['Bande72']+df['Bande106'])
    df['NDRE'] = (df['Bande102']-df['Bande84'])/(df['Bande102']+df['Bande84'])
    df['PSRI'] = (df['Bande72']-df['Bande24'])/df['Bande92']
    

    return df

def add_lidar_data(plotid):
    """Add lidar data to the dataframe"""
    
    df = add_indices(plotid)
    df_lidar = pd.read_csv(r"lidar_features/raster/img_{}.csv".format(plotid))
    df_lidar = df_lidar.rename(columns={'plot': 'plotid','0':'row', '1':'col'})

    
    #merge
    df_merged = df.merge(df_lidar, on=['row', 'col'])
    
    #plotid in str rather than int
    df_merged['plotid'] = df_merged['plotid'].astype(str)
    
    #bring 'plotid' column in front
    plot = df_merged['plotid']
    df_merged.drop(labels=['plotid'], axis=1,inplace = True)
    df_merged.insert(0, 'plotid', plot)
    
    
    return df_merged



    

    
    
    


                       
    
    
    
    
    
    