"Map seul" correspond au code pour faire un plot d'une carte sur un jour.

"traitement2" correspond au calcul de la nouvelle discrétisation et de la somme spatial des pixels.

"cartes2" correspond au plot de la map moyenne issue du traitement réalisé dans "traitement2".